const path = require("path")

const UglifyJsPlugin = require("uglifyjs-webpack-plugin")

module.exports = {
    entry: {
        "index": path.resolve(__dirname, "frontend/js/index.js"),
        "stripe": path.resolve(__dirname, "frontend/js/stripe.js"),
        "table": path.resolve(__dirname, "frontend/js/table.js"),
        "frame": path.resolve(__dirname, "frontend/css/frame.sass")
    },
    output: {
        path: path.resolve(__dirname, "static"),
        filename: "[name].js",
        publicPath: "/static/"
    },
    optimization: {
        minimizer: [new UglifyJsPlugin({
            parallel: true,
            uglifyOptions: {
                drop_console: true
            }
        })]
    },
    resolve: {
        alias: {
            "~css": path.resolve(__dirname, "./frontend/css"),
            "~fonts": path.resolve(__dirname, "./frontend/fonts"),
            "~js": path.resolve(__dirname, "./frontend/js")
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader"
                ]
            },
            {
                test: /\.s(a|c)ss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|png|jpg|jpeg|gif)/,
                use: [
                    { loader: "file-loader", options: { name: "[name].[ext]" } }
                ]
            },
            {
                test: /\.js$/,
                use: [
                    { loader: "babel-loader", options: {
                        presets: ["@babel/preset-env"],
                        plugins: ["@babel/plugin-syntax-dynamic-import"]
                    } }
                ]
            }
        ]
    }
}