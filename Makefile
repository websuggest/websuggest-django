setup:
	# Set up Python
	pip3 install poetry
	poetry install

	# Set up Node.js
	npm install -g yarn
	yarn install

	# Seed the database
	poetry run python manage.py seed

webpack-dev:
	# Webpack dev server
	yarn dev

django-dev:
	# Django dev server
	poetry run python3 manage.py runserver

dev:
	# Run Webpack and Django dev servers in parallel
	make -j2 webpack-dev django-dev

test:
	poetry run python manage.py test

build:
	# Build assets for production using Webpack
	yarn build
