import os
from pathlib import Path

from django_magic.settings import SettingsManager

base_dir = Path(__file__).parent.parent.absolute()
s = SettingsManager(base_dir)

#
# Settings for all environments.
#

ALLOWED_HOSTS = s.env("DJANGO_ALLOWED_HOSTS", [
    "*"
])
INSTALLED_APPS = s.env("DJANGO_INSTALLED_APPS", [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "app",
    "crispy_forms",
    "mathfilters",
    "django_extensions"
])
MIDDLEWARE = s.env("DJANGO_MIDDLEWARE", [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "htmlmin.middleware.HtmlMinifyMiddleware",
    "htmlmin.middleware.MarkRequestMiddleware"
])
ROOT_URLCONF = s.env("DJANGO_ROOT_URLCONF", "websuggest.urls")
TEMPLATES = s.env("DJANGO_TEMPLATES", [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [s.relative_path("templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "builtins": [
                "django.templatetags.static"
            ]
        },
    },
])
WSGI_APPLICATION = s.env(
    "DJANGO_WSGI_APPLICATION",
    "websuggest.wsgi.application"
)
DATABASES = {
    "default": s.db("DATABASE_URL", "db.sqlite3")
}
AUTH_PASSWORD_VALIDATORS = s.env("DJANGO_AUTH_PASSWORD_VALIDATORS", [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
])
LANGUAGE_CODE = s.env("DJANGO_LANGUAGE_CODE", "en-us")
TIME_ZONE = s.env("DJANGO_TIME_ZONE", "UTC")
USE_I18N = s.env("DJANGO_USE_I18N", True)
USE_L10N = s.env("DJANGO_USE_L10N", True)
USE_TZ = s.env("DJANGO_USE_TZ", True)
STATIC_URL = s.env("STATIC_URL", "/static/")
STATICFILES_DIRS = s.env(
    "DJANGO_STATICFILES_DIRS",
    (s.relative_path("static"), )
)
LOGIN_REDIRECT_URL = s.env("DJANGO_LOGIN_REDIRECT_URL", "home")
LOGOUT_REDIRECT_URL = s.env("DJANGO_LOGOUT_REDIRECT_URL", "home")
CRISPY_TEMPLATE_PACK = s.env("CRISPY_TEMPLATE_PACK", "bootstrap4")
GULP_DEVELOP_COMMAND = s.env("GULP_DEVELOP_COMMAND", "gulp")
GULP_PRODUCTION_COMMAND = s.env("GULP_PRODUCTION_COMMAND", "MODE=prod gulp build")
EMAIL_BACKEND = s.env(
    "DJANGO_EMAIL_BACKEND",
    "django.core.mail.backends.smtp.EmailBackend"
)
DEFAULT_FROM_EMAIL = s.env(
    "DJANGO_DEFAULT_FROM_EMAIL",
    "bot@websuggest.net"
)
SERVER_EMAIL = s.env(
    "DJANGO_SERVER_EMAIL",
    "bot@websuggest.net"
)

#
# Settings that are environment-specific.
#

HTML_MINIFY = s.env("HTML_MINIFY", False)
DEBUG = s.env("DJANGO_DEBUG", True)
SECRET_KEY = s.env(
    "DJANGO_SECRET_KEY",
    "vk%5+-widw&8*&s*b4bc7qkb!#ziqjjxt)t(&ti6am5$&4xg9^"
)
SECURE_SSL_REDIRECT = s.env("DJANGO_SECURE_SSL_REDIRECT", False)
STRIPE_CLIENT_ID = s.env("STRIPE_CLIENT_ID")
STRIPE_PUBLIC_KEY = s.env("STRIPE_PUBLIC_KEY")
STRIPE_SECRET_KEY = s.env("STRIPE_SECRET_KEY")
EMAIL_HOST = s.env("DJANGO_EMAIL_HOST", "localhost")
EMAIL_USE_TLS = s.env("DJANGO_EMAIL_USE_TLS", False)
EMAIL_PORT = s.env("DJANGO_EMAIL_PORT", 1025)
EMAIL_HOST_USER = s.env("SENDGRID_USERNAME", "")
EMAIL_HOST_PASSWORD = s.env("SENDGRID_PASSWORD", "")

#
# Settings only applicable in production.
#
STATIC_ROOT = s.env("DJANGO_STATIC_ROOT", s.relative_path("staticfiles"))
STATICFILES_STORAGE = s.env("DJANGO_STATICFILES_STORAGE", "django.contrib.staticfiles.storage.ManifestStaticFilesStorage")
# password protect staging application
if s.env("PASSWORD_PROTECT", False):
    BASICAUTH_USERS = {
        os.environ["HTTP_USERNAME"] : os.environ["HTTP_PASSWORD"]
    }
    MIDDLEWARE += ('basicauth.middleware.BasicAuthMiddleware',)

#
# Settings only applicable in development.
#
