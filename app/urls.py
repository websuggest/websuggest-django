
from django_magic.routing import RoutingManager
from app import views

r = RoutingManager(views)

urlpatterns = [
    r.get_view("signup"),
    r.get_view("create_website"),
    r.get_class_view("create_advertisement"),
    r.get_view("frame", url="frame/<int:website_id>"),
    r.get_view("click"),
    r.get_view("advertisement", url="advertisement/<int:ad_id>"),
    r.get_view("website", url="website/<int:website_id>"),
    r.get_view("pause_advertisement", url="toggle/<int:ad_id>"),
    r.get_view("stripe_success"),
    r.get_view("stripe_connect"),
    r.get_view("stripe_elements"),
    r.get_view("stripe_setup_advertiser"),
    r.get_view("clicks_api")
]
