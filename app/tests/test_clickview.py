import datetime

from django.test import RequestFactory, TestCase

from app.models import AdViewer
from app.views.ClickView import ClickfraudProtect


class ClickViewTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.factory.defaults["SERVER_NAME"] = "beta.websuggest.net"

    def test_clickfraud_referer_correct(self):
        request = self.factory.get(
            "/click?w_id=1&ad_id=1",
            HTTP_REFERER="http://beta.websuggest.net/frame/1"
        )
        instance = ClickfraudProtect(request, "192.0.2.0")
        result = instance.referer_is_incorrect(1)
        self.assertFalse(result)

    def test_clickfraud_referer_incorrect(self):
        request = self.factory.get(
            "/click?w_id=1&ad_id=1",
            HTTP_REFERER="https://example.com"
        )
        instance = ClickfraudProtect(request, "192.0.2.0")
        result = instance.referer_is_incorrect(1)
        self.assertTrue(result)

    def test_clickfraud_first_click(self):
        request = self.factory.get("/click?w_id=1&ad_id=1")
        instance = ClickfraudProtect(request, "192.0.2.0")
        result = instance.user_is_spammer()
        with self.subTest():
            self.assertFalse(result)
        with self.subTest():
            adviewer = AdViewer.objects.get(ip="192.0.2.0")
            self.assertEqual(adviewer.ad_clicks, 1)

    def test_clickfraud_frequency_ok(self):
        request = self.factory.get("/click?w_id=1&ad_id=1")
        instance = ClickfraudProtect(request, "192.0.2.0")
        yesterday = datetime.date.fromordinal(datetime.date.today().toordinal() - 1)
        AdViewer.objects.create(last_click=yesterday, ad_clicks=2, ip="192.0.2.0")
        self.assertFalse(instance.user_is_spammer())

    def test_clickfraud_too_many_clicks(self):
        request = self.factory.get("/click?w_id=1&ad_id=1")
        instance = ClickfraudProtect(request, "192.0.2.0")
        yesterday = datetime.date.fromordinal(datetime.date.today().toordinal() - 1)
        AdViewer.objects.create(last_click=yesterday, ad_clicks=1000, ip="192.0.2.0")
        self.assertTrue(instance.user_is_spammer())

    def test_clickfraud_too_recent(self):
        request = self.factory.get("/click?w_id=1&ad_id=1")
        instance = ClickfraudProtect(request, "192.0.2.0")
        one_second_ago = datetime.datetime.now() - datetime.timedelta(seconds=10)
        AdViewer.objects.create(last_click=one_second_ago, ad_clicks=2, ip="192.0.2.0")
        self.assertTrue(instance.user_is_spammer())
