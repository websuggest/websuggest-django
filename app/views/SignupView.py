from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render

from ..forms import UserCreateForm


def SignupView(request):
    if request.method == "POST":
        form = UserCreateForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            form.save()
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = UserCreateForm()
    return render(request, "signup.html", {
        "form": form
    })
