from django.shortcuts import redirect, render

from lib.logic.total_balance import total_balance

from ..models import Advertisement


def AdvertisementView(request, ad_id):
    if request.user.is_authenticated:
        ad_object = Advertisement.objects.get(id=ad_id)
        if ad_object.user == request.user.profile:
            total_clicks = 0
            for click in ad_object.adclick_set.all():
                total_clicks += 1
            if total_clicks != 0:
                conversion_rate_raw = (total_clicks / ad_object.views) * 100
                # Round the conversion rate
                conversion_rate = round(conversion_rate_raw, 1)
            else:
                conversion_rate = 0
            return render(request, "advertisement_view.html", {
                "ad": ad_object,
                "total_clicks": total_clicks,
                "conversion_rate": conversion_rate,
                "total_balance": total_balance(request.user.profile)
            })
        else:
            return redirect("home")
    else:
        return redirect("home")
