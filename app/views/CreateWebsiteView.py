from django.shortcuts import redirect, render

from ..forms import CreateWebsiteForm


def CreateWebsiteView(request):
    if request.method == "POST":
        form = CreateWebsiteForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.user = request.user.profile
            model.save()
            return redirect("website", website_id=str(model.id))
    else:
        form = CreateWebsiteForm()
        return render(request, "create_website.html", {
            "form": form
        })
