import requests
from django.conf import settings
from django.shortcuts import redirect


def StripeSetupAdvertiserView(request):
    if request.method == "POST" and request.POST.get("stripeToken", None):
        customer = requests.post(
            "https://api.stripe.com/v1/customers",
            auth=(settings.STRIPE_SECRET_KEY, ""),
            data={"source": request.POST["stripeToken"]}
        )
        if customer.status_code == 200:
            user = request.user
            user.profile.stripe_customer = customer.text
            user.save()
            return redirect("create_advertisement")
        else:
            return redirect("stripe_elements")
    else:
        return redirect("stripe_elements")
