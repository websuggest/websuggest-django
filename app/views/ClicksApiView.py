from django.db.models import Count
from django.db.models.functions import TruncMonth
from django.http import JsonResponse
from dateutil.relativedelta import relativedelta
from datetime import datetime

from app.models import Advertisement, Website


def month_range(start=None, end=datetime.today()):
    if not start:
        start = datetime(end.year - 1, end.month, 1)
    result = []
    current = start
    while current <= end:
        result.append(current)
        current += relativedelta(months=1)
    return result


def value_for_month(month, query_set):
    if query_set.filter(x=month).exists():
        return query_set.get(x=month)['y']
    else:
        return 0


def ClicksApiView(request):
    for_ad = request.GET.get("for_ad", None)
    for_website = request.GET.get("for_website", None)
    if for_ad:
        ad_obj = Advertisement.objects.get(id=for_ad)
        if request.user.is_authenticated and ad_obj.user == request.user.profile:
            # calculate total clicks
            total_clicks = 0
            for click in ad_obj.adclick_set.all():
                total_clicks += 1
            # calculate time-series
            time_series = ad_obj.adclick_set.annotate(x=TruncMonth('datetime')).values('x').annotate(y=Count('id')).values('x', 'y').order_by()
            months = month_range()
            graph_data = {
                m.strftime('%Y-%m-01'): value_for_month(m, time_series) for m in months
            }
            return JsonResponse({
                "clicks": [
                    {
                        "location": adclick.location,
                        "website": {
                            "url": adclick.website.url
                        },
                        "price": adclick.price,
                        "datetime": adclick.datetime,
                        "id": adclick.id
                    } for adclick in ad_obj.adclick_set.all()
                ],
                "total_views": ad_obj.views,
                "total_clicks": total_clicks,
                "time_series": graph_data
            })
        else:
            return JsonResponse({"error": 403}, status=403)
    elif for_website:
        website_obj = Website.objects.get(id=for_website)
        if request.user.is_authenticated and website_obj.user == request.user.profile:
            # calculate total clicks
            total_clicks = 0
            for click in website_obj.adclick_set.all():
                total_clicks += 1
            # calculate time-series
            time_series = website_obj.adclick_set.annotate(x=TruncMonth('datetime')).values('x').annotate(y=Count('id')).values('x', 'y').order_by()
            months = month_range()
            graph_data = {
                m.strftime('%Y-%m-01'): value_for_month(m, time_series) for m in months
            }
            return JsonResponse({
                "clicks": [
                    {
                        "location": adclick.location,
                        "website": {
                            "url": adclick.website.url
                        },
                        "price": adclick.price,
                        "datetime": adclick.datetime,
                        "id": adclick.id
                    } for adclick in website_obj.adclick_set.all()
                ],
                "total_views": website_obj.views,
                "total_clicks": total_clicks,
                "time_series": graph_data
            })
        else:
            return JsonResponse({"error": 403}, status=403)
    else:
        return JsonResponse({"error": 404}, status=404)
