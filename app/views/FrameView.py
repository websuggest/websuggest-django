from random import randint

from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt

from ..models import Advertisement, Website


@xframe_options_exempt
def FrameView(request, website_id):
    ads = Advertisement.objects.filter(passed=True, paused=False)
    count = ads.count()
    if count == 0:
        return render(request, "frame.html", {
            "object": {
                "title": "This could be your ad...",
                "body": "WebSuggest is a privacy-respecting, easy and simple way to advertise across an entire network of websites."
            },
            "website_id": None
        })
    random_object = ads[randint(0, count - 1)]
    website_object = Website.objects.get(id=website_id)
    website_object.views += 1
    website_object.save()
    random_object.views += 1
    random_object.save()
    return render(request, "frame.html", {
        "object": random_object,
        "website_id": website_object.id
    })
