from django.shortcuts import redirect

from ..models import Advertisement


def PauseAdvertisementView(request, ad_id):
    ad = Advertisement.objects.get(id=ad_id)
    if ad.paused:
        ad.paused = False
        ad.save()
        return redirect("advertisement", str(ad.id))
    else:
        ad.paused = True
        ad.save()
        return redirect("advertisement", str(ad.id))
