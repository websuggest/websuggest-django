# Generated by Django 2.0.4 on 2018-05-06 22:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20180421_2133'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='is_stripe_connected',
            field=models.BooleanField(default=False),
        ),
    ]
