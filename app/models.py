from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from jsonfield import JSONField
from decimal import Decimal


class Website(models.Model):
    url = models.URLField()
    user = models.ForeignKey('Profile', on_delete=models.CASCADE)
    views = models.IntegerField(default=0)

    def __str__(self):
        return "({}) {}".format(self.id, self.url)


class Advertisement(models.Model):
    title = models.CharField(max_length=20)
    body = models.TextField(max_length=180)
    user = models.ForeignKey('Profile', on_delete=models.CASCADE)
    passed = models.BooleanField(default=False)
    target_url = models.URLField('URL to open when ad is clicked')
    paused = models.BooleanField(default=False)
    views = models.IntegerField(default=0)

    def __str__(self):
        if self.passed:
            return "({}) {} - {}".format(self.id, self.title, self.target_url)
        else:
            return "({}) {} - {} [AWAITING REVIEW]".format(
                self.id,
                self.title,
                self.target_url
            )

class Transaction(models.Model):
    def advertisement_charge_amount(self):
        result = Decimal(0)
        for adclick in self.advertisement_charge_adclick_set.all():
            result += adclick.price
        return result

class AdClick(models.Model):
    advertisement = models.ForeignKey(Advertisement, on_delete=models.CASCADE)
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    location = JSONField()
    datetime = models.DateTimeField()
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    advertisement_charge_transaction = models.ForeignKey(Transaction,
        on_delete=models.SET_NULL, null=True, related_name='advertisement_charge_adclick_set')


class AdViewer(models.Model):
    ip = models.GenericIPAddressField()
    ad_clicks = models.IntegerField(default=0)
    last_click = models.DateTimeField()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    stripe_user_id = models.CharField(default="0", max_length=10000)
    stripe_customer = models.CharField(default="0", max_length=10000)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
